package com.example.mytestproject.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.mytestproject.R
import com.example.mytestproject.databinding.ActivityCoRoutinConceptBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CoRoutinConceptActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityCoRoutinConceptBinding
    private var count:Int=0

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
       binding= DataBindingUtil.setContentView(this,R.layout.activity_co_routin_concept)
        binding.btnCount.setOnClickListener(this)
        binding.btnDownload.setOnClickListener(this)
    }

    override fun onClick(v : View?) {
        when(v?.id){
            R.id.btn_count->{
            count++
            binding.tvCount.text=count.toString()
            }
            R.id.btn_download->{
              downloading()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun downloading() {
        CoroutineScope(Dispatchers.IO).launch {
            for (i in 1..5000000) {
                withContext(Dispatchers.Main) {
                    binding.tvDownloading.text = "Downloading...$i"
                }
            }
            binding.tvDownloading.text = "Successfully....."
        }
    }

}